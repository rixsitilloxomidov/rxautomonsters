import React from "react";
import './App.css';
import Routes from "./routes/Routes";
import "mdbreact/dist/css/mdb.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "tailwindcss/dist/base.css";

function App() {
    return (
        <div className="App">
            <Routes/>
        </div>
    );
}

export default App;
