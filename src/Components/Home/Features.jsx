import React from 'react';

import defaultCardImage from "../../Assets/SVG/shield-icon.svg";
import SupportIconImage from "../../Assets/SVG/support-icon.svg";
import ShieldIconImage from "../../Assets/SVG/shield-icon.svg";
import CustomizeIconImage from "../../Assets/SVG/customize-icon.svg";
import styled from "styled-components";

export default ({
                    cards = null,
                    heading = "Amazing Features",
                    subheading = "",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit," +
                    " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                }) => {

    const defaultCards = [
        {
            imageSrc: ShieldIconImage,
            title: "Secure",
            description:
                "No haggle pricing if the vehicle is in the condition you told us its in",
        },
        {
            imageSrc: SupportIconImage,
            title: "24/7 Support",
            description: "Pick up near you",
        },
        {
            imageSrc: CustomizeIconImage,
            title: "Customizable",
            description: "Better offer than your local dealerships",
        },
    ];

    if (!cards) cards = defaultCards;

    return (
        <FeaturesWrapper>
            <div className="container">
                <div className="features-top">
                    <h1 className="features-heading">{heading}</h1>
                    <p className="features-subheading">{description}</p>
                </div>
                <div className="row no-gutters">
                    {cards.map((card, index) => (
                        <div key={index} className="col-md-4">
                            <div className="feature-card">
                                <div className="card-img">
                                    <img src={card.imageSrc || defaultCardImage} alt={card.title}/>
                                </div>
                                <div className="card-title">
                                    {card.title}
                                </div>
                                <div className="card-subtitle">
                                    {card.description}
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </FeaturesWrapper>
    );
};
const FeaturesWrapper = styled.div`
background:#180b7d;
color: lavender;
padding-bottom: 2rem;
  .features-top{
    display:block;
   text-align: center;
   .features-heading{
     font-size: 3rem;
     padding: 2rem;
   }
   .features-subheading{
    padding-bottom: 2rem;
    font-size: 1.2rem;
    width: 60%;
    margin: 0 auto;
   }
  }
  .card-img{
    width: 4rem;height: 4rem;
    background:lavender;
    border-radius: 50%;
    margin: 1rem;
    display:flex;
    justify-content:center;
    align-items: center;
  }
  .feature-card{
    padding: 2rem 0;
    text-align: left;
    .card-title{
      font-size: 1.4rem;
    }
  }
`;
