import React from 'react';

import styled from "styled-components";
import tw from "twin.macro";
//import {MDBNavbar, MDBNavbarBrand} from "mdbreact";
//const Container = tw.div`relative`;
import tesla from "../../Assets/tesla.png"
import Func from "./SendFunc/Func";
import Features from "./Features";

//const Heading = tw.h1`font-bold text-3xl md:text-3xl lg:text-4xl xl:text-5xl text-gray-500 leading-tight`;

export default function Home(props) {
    return (
        <div>
            <HomeWrapper id="back">
                <div className="container">
                    <div className="row no-gutters">
                        <div className="col-md-6">
                            <h1 className="h1 heading">Delivering, now and always.</h1>
                            <p className="paragraph">
                                Buy a car entirely online, and have it safely delivered,
                                contact-free.
                            </p>
                            <Func/>
                        </div>
                        <div className="col-md-6">
                            <div className="img-container">
                                <img src={tesla} alt="your car"/>
                            </div>
                        </div>
                    </div>
                </div>

                <Features/>

            </HomeWrapper>
        </div>
    );
}

const HomeWrapper = styled.div`
  margin-top: 6rem;
  .heading{
    font-size: 3rem;
    color: lightslategray;
    margin: 2rem 0;
    font-weight:bold;
    text-align: left;
  }
  .img-container{
    width: 100%;
    height: 90vh;
    display:flex;
    align-items: center;
    img{
      
    }
  }
  #back {
  background: linear-gradient(-80deg, white 49.9%, #7b88ab 50%);
  
 }
 #back2 {
   background: linear-gradient(-80deg, #7b88ab 49.9%, white 50%);
 }
 #GColor {
   background-color: #7b88ab;
 }

`;